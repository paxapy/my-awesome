#!/usr/bin/env bash

nm-applet &
setxkbmap -layout "us,ru" -variant "colemak," "grp:caps_toggle" &

/usr/libexec/geoclue-2.0/demos/agent &
redshift &

